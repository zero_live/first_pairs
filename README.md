# FIRST PAIRS

A litle software for make random code pairs with two roles, Driver and navigator. We are doing this software because we always try to do pair programming with the people that we feel more confortable.

## System requirements

For development you only need a modern web browser that support ES6.

For run test end to end:

- `Docker version 20` or compatible versions.
- `docker-compose 1.29` or compatible versions

## How to run the tests

- Run unit tests by openning in your browser the file `Verano.html`

- For run all the tests (end to end and unit) use: `sh run-all-tests.sh`

## How to run the application

- Open in your browser the file `index.html`

## Where can you try this app?

- Clicking in [First Pairs app](https://zero_live.gitlab.io/first_pairs/)
