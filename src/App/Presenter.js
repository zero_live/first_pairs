const Presenter = class {
  static pairAsHtml(pair) {
    return `
      <ul>
          <li>Driver: ${pair.driver}</li>
          <li>Navigator: ${pair.navigator}</li>
      </ul>
    `
  }

  static personAsHtml(name) {
    const removeButton = `<a href="#" onclick="Actions.removePersonByName('${name}')">🗑️</a>`

    return `<li>${name} ${removeButton}</li>`
  }

  static cleanPeopleList() {
    const peopleList = document.querySelector("#people")

    peopleList.innerHTML = ''
  }

  static appendPeopleList(person) {
    const peopleList = document.querySelector("#people")

    peopleList.innerHTML += person
  }

  static cleanPairList() {
    const pairList = document.querySelector("#pairs")

    pairList.innerHTML = ''
  }

  static appendPairList(pair) {
    const pairList = document.querySelector("#pairs")

    pairList.innerHTML += pair
  }
}
