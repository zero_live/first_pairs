
const PeopleRepository = class {
  static COLLECTION = 'people'
  static retrieveAll() {
    let people = []

    const peopleAsJson = localStorage.getItem(this.COLLECTION)
    if (peopleAsJson !== null) {
      people = JSON.parse(peopleAsJson)
    }

    return people
  }

  static add(person) {
    const people = this.retrieveAll()
    people.push(person)
    this._store(people)
  }

  static remove(name) {
    let people = this.retrieveAll()
    people = people.filter(person => person !== name)
    this._store(people)
  }

  static _store(people) {
    const peopleAsJson = JSON.stringify(people)

    localStorage.setItem(this.COLLECTION, peopleAsJson)
  }
}
