
const Drawer = class {
  static drawPeople() {
    Presenter.cleanPeopleList()
    const people = PeopleRepository.retrieveAll()
    people.forEach((person) => {
      const personAsHtml = Presenter.personAsHtml(person)

      Presenter.appendPeopleList(personAsHtml)
    })
  }

  static drawPairs() {
    const people = PeopleRepository.retrieveAll()
    const pairs = FirstPairs.generateRandomListWith(people)

    Presenter.cleanPairList()
    pairs.forEach((pair) => {
      const pairAsHtml = Presenter.pairAsHtml(pair)

      Presenter.appendPairList(pairAsHtml)
    })
  }
}
