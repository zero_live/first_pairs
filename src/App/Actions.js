
const Actions = class {
  static addPersonToPeople(event) {
    const person = event.target.value
    if (!person) { return }

    const isEnterPressed = (event.key === 'Enter')
    if (isEnterPressed) {
      PeopleRepository.add(person)
      Actions.listPeople()
      event.target.value = ''
    }
  }

  static removePersonByName(person) {
    PeopleRepository.remove(person)
    Actions.listPeople()
  }

  static listPairs() {
    Drawer.drawPairs()
  }

  static listPeople() {
    Drawer.drawPeople()
  }
}
