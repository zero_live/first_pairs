
const App = class {
  static load() {
    this._listPeopleOnLoad()
    this._addPersonFromInputOnEnter()
    this._generatePairsOnClickInButton()
  }

  static _listPeopleOnLoad() {
    const app = window

    app.addEventListener('load', Actions.listPeople)
  }

  static _addPersonFromInputOnEnter() {
    const inputForNewPerson = document.querySelector('#new-person')

    inputForNewPerson.addEventListener('keyup', Actions.addPersonToPeople)
  }

  static _generatePairsOnClickInButton() {
    const buttonForGeneratePairs = document.querySelector('#generate-pairs')

    buttonForGeneratePairs.addEventListener('click', Actions.listPairs)
  }
}
