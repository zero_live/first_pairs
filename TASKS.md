# TASKS

## First Pairs generator

- [x] Generate an EMPTY pair list when retrieves an EMPTY people list.
- [x] Generate a pair list with a pair where the driver and the navigator are the same person when retrieves a list with A PERSON.
- [x] Generate a pair list with a pair where a person is the driver and another one is the navigator when retrieves a list with TWO persons.
- [x] Generate a pair list with two pairs when retrieves a list with THREE persons.
- [ ] ~~Generate a pair list with two pairs when retrieves a list with FOUR persons~~.
- [ ] ~~Generate a pair list with three pairs where one of them only has a person when retrieves a list with FIVE persons~~.
- [x] Generate a pair list randomly.

## Presentation

- [x] Show a button for generate pairs
- [x] When the button is pressed, show a list of pairs
- [x] The list of pair show the role of every person.
- [x] Show the list of people for do pair programming.
- [ ] ~~Be able to check the people that are for pair~~.
- [x] Be able to remove people from the initial list.
- [x] Be able to add people to the initial list.
- [x] Store the people in the local storage.
- [x] Style the page.
  - [ ] Style the page truly :_(
- [x] Create script to run e2e tests.
- [x] Add favicon.
- [x] Refartor `index.js`.

## Testing e2e

- [x] Test with end to end.
- [x] Move FirstPairsSpec.js to spec/src
- [x] Complete all test e2e.

## Deployment

- [x] Run tests in before deploy.
- [x] Deploy to gitlab pages
