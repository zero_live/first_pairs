
describe('FirstPairs', () => {
  it('generates an empty pair list when retrieves an empty people list', () => {
    const people = []

    const pairs = FirstPairs.generateRandomListWith(people)

    expect(pairs).toBe([])
  })

  it('generates a pair list with a pair where the driver and the navigator are the same person when retrieves a list with a person', () => {
    const aPerson = 'Akira'

    const pairs = FirstPairs.generateRandomListWith([aPerson])

    expect(pairs).toBe([{ driver: aPerson, navigator: aPerson }])
  })

  it('generates a pair list randomly', () => {
    const aPerson = 'Akira'
    const anotherPerson = 'Charlie'
    const anotherPersonMore = 'Sam'
    const people = [aPerson, anotherPerson, anotherPersonMore]

    const pairs = FirstPairs.generateRandomListWith(people)

    expect(pairs.length).toBe(2)
    const driver = pairs[0].driver
    expect(people).include(driver)
    const navigator = pairs[0].navigator
    expect(people).include(navigator)
    const anotherDriver = pairs[1].driver
    expect(people).include(anotherDriver)
    const anotherNavigator = pairs[1].navigator
    expect(people).include(anotherNavigator)
  })
})
