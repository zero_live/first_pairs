
describe('First Pairs', () => {
  beforeEach(() => {
    cy.clearLocalStorage()
  })

  it('generates a list of pairs from added people', () => {
    cy.visit("./index.html")
    cy.get("#new-person").type("Akira{enter}")
    cy.get("#new-person").type("Chalie{enter}")

    cy.get("#generate-pairs").click()

    cy.get("#pairs").contains("Driver: ")
    cy.get("#pairs").contains("Navigator: ")
    cy.get("#pairs").contains("Akira")
    cy.get("#pairs").contains("Chalie")
  })

  it('can delete a person', () => {
    cy.visit("./index.html")
    cy.get("#new-person").type("Akira{enter}")

    cy.get("a").click()

    cy.get("#people").should('be.empty')
  })

  it('generates a pair with a person', () => {
    cy.visit("./index.html")
    cy.get("#new-person").type("Akira{enter}")

    cy.get("#generate-pairs").click()

    cy.get("#pairs").contains("Driver: Akira")
    cy.get("#pairs").contains("Navigator: Akira")
  })
})
