
describe('Unit tests', () => {
  it('have no failures', () => {
    cy.visit('Verano.html')

    cy.get('.error-message').should('not.exist')
    cy.get('.fails').should('not.exist')
    cy.get('.skips').should('not.exist')
    cy.contains('Test passed: ')
  })
})
